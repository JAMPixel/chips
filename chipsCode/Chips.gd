extends AnimatedSprite

# Declare member variables here. Examples:
var proba = 0.01
var mouv = 0
var etatMouv = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var rand = 1
	
	if(mouv == 0):
		rand = rand_range(0, 2)
	
	if(mouv != 0 && rand<proba):
		mouv = randi()%4 + 1
	else:
		if(mouv == 1):
			self.offset.y += 0.5
		if(mouv == 2):
			self.offset.x -= 0.5
		if(mouv == 3):
			self.offset.y -= 0.5
		if(mouv == 4):
			self.offset.x += 0.5
		etatMouv += 1
		
		if(etatMouv >= 100):
			etatMouv = 0
			mouv = 0
	pass
