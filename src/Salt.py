import pygame
from pygame.locals import *
from Rain import *
from Button import *

class Salt(pygame.sprite.Sprite):
    def __init__(self, position, nb_bat):
        pygame.sprite.Sprite.__init__(self)

        self.image=pygame.Surface([128,128])
        img=pygame.image.load("../asset/decor/salt.png").convert_alpha()
        self.imgbleu=pygame.Surface([130,130])
        self.imgbleu=pygame.image.load("../asset/decor/fondbleu.png").convert_alpha()
        self.imgrouge=pygame.Surface([130,130])
        self.imgrouge=pygame.image.load("../asset/decor/fondrouge.png").convert_alpha()

        self.rect=self.image.get_rect(center=(position[0],position[1]))
        self.rectbl=self.imgbleu.get_rect(center=(position[0],position[1]))
        self.rectrg=self.imgrouge.get_rect(center=(position[0],position[1]))
        self.image=img
        self.x=self.rect.x
        self.y=self.rect.y

        self.cout = 10 + 8*nb_bat
        self.rain=None
        self.value = 0.075

    def update(self):
        if (self.rain==None):
            return(self.value)
        else:
            if (self.rain.update()):
                self.rain=None
            return 0


    def draw(self, fenetre, position,b,joueur):
        if(b):
            fenetre.blit(self.imgbleu,(self.x-position[0]-1,self.y-position[1]-1))
        else:
            fenetre.blit(self.imgrouge,(self.x-position[0]-1,self.y-position[1]-1))
        fenetre.blit(self.image,(self.x-position[0],self.y-position[1]))
        if joueur.umbrella:
            umbrella = pygame.image.load("../asset/chips/parapluie.png").convert_alpha()
            fenetre.blit(umbrella,(self.x-position[0]+30,self.y-position[1]))
        elif (self.rain!=None):
            self.rain.draw(fenetre,position)

    def addRain(self):
        if (self.rain==None):
            self.rain=Rain([self.x+64,self.y+64])

    def onClick(self, fenetre, font, joueur):
        pause=True
        cout_umbrella = 20
        cout_cage = 50
        tempo = 0
        max_tempo=15

        if not joueur.umbrella or not joueur.cloudCage:
            while pause:
                pygame.time.Clock().tick(60)
                tempo+=1
                if tempo>max_tempo:
                    tempo=max_tempo
                for event in pygame.event.get():
                    if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
                        pause = False

                txt_cout = ""
                txt_explication = ""
                txt_explication2 = ""
                txt_explication3 = ""
                txt_explication4 = ""
                panneau = pygame.image.load("../asset/HUD/panneau.png").convert_alpha()
                buttonUmbrella= Button((230,260),"../asset/HUD/btnparapluie.png","../asset/HUD/btnparapluiehover.png")
                buttonCage= Button((230,350),"../asset/HUD/btnnuage-cage.png","../asset/HUD/btnnuage-cagehover.png")
                imgChips = pygame.image.load("../asset/chips/chipsFace1.png").convert_alpha()

                mous_g,mous_m,mous_d=pygame.mouse.get_pressed()
                mouse_pos=pygame.mouse.get_pos()

                if (mous_g):
                    if joueur.umbrella == False and buttonUmbrella.on_click(mouse_pos) and len(joueur.m_chips) >= cout_umbrella:
                        pause = False
                        joueur.umbrella = True
                        for i in range(cout_umbrella):
                            joueur.m_chips.pop()
                    elif joueur.cloudCage == False and joueur.umbrella and buttonCage.on_click(mouse_pos) and len(joueur.m_chips) >= cout_cage:
                        pause = False
                        joueur.cloudCage = True
                        for i in range(cout_cage):
                            joueur.m_chips.pop()
                    elif not(panneau.get_rect().collidepoint(mouse_pos)) and tempo==max_tempo:
                        pause = False


                fenetre.blit(panneau, (200, 200))

                if joueur.umbrella == False:
                    txt_cout = ": -" + str(cout_umbrella)
                    txt_explication = "Le parapluie protege"
                    txt_explication2 = "le sel de la pluie"
                    buttonUmbrella.draw(fenetre)

                    render_cout = font.render(txt_cout, 1, (255,0,0))
                    render_explication = font.render(txt_explication, 1, (255,255,255))
                    render_explication2 = font.render(txt_explication2, 1, (255,255,255))

                    fenetre.blit(imgChips, (260, 240))
                    fenetre.blit(render_cout, (300, 250))
                    fenetre.blit(render_explication, (260, 275))
                    fenetre.blit(render_explication2, (260, 300))

                if joueur.cloudCage == False:
                    txt_cout = ": -" + str(cout_cage)
                    txt_explication = "La cage capture les "
                    txt_explication2 = "nuages et permet de"
                    txt_explication3 = "les utiliser contre"
                    txt_explication4 = "l'adversaire"

                    buttonCage.draw(fenetre)

                    render_cout = font.render(txt_cout, 1, (255,0,0))
                    render_explication = font.render(txt_explication, 1, (255,255,255))
                    render_explication2 = font.render(txt_explication2, 1, (255,255,255))
                    render_explication3 = font.render(txt_explication3, 1, (255,255,255))
                    render_explication4 = font.render(txt_explication4, 1, (255,255,255))
                    fenetre.blit(imgChips, (260, 330))
                    fenetre.blit(render_cout, (300, 340))
                    fenetre.blit(render_explication, (260, 365))
                    fenetre.blit(render_explication2, (260, 390))
                    fenetre.blit(render_explication3, (260, 415))
                    fenetre.blit(render_explication4, (260, 440))
                else:
                    txt_cout = ""
                    txt_explication = ""
                    txt_explication2 = ""



                pygame.display.flip()
