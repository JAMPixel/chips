import pygame
from Factory import *
from Salt import *
from Field import *

class Participant:

    def __init__(self):
        self.m_patate = 10
        self.m_sel = 10

        self.m_chips = []

        self.m_champs = []
        self.m_salliere = []
        self.m_usine = []

        self.img_patate=pygame.Surface([32,32])
        img=pygame.image.load("../asset/decor/potato.png").convert_alpha()
        self.img_patate=img

        self.img_sel=pygame.Surface([32,32])
        img=pygame.image.load("../asset/decor/Sel.png").convert_alpha()
        self.img_sel=img

        self.img_chips=pygame.Surface([32,32])
        img=pygame.image.load("../asset/chips/chipsFace1.png").convert_alpha()
        self.img_chips=img

        self.scarcrow = False
        self.ravenCage = False

        self.umbrella = False
        self.cloudCage = False

        self.m_corbeau=0
        self.m_nuage=0

    def newFactory(self,position):
        usine= Factory(position, len(self.m_usine))
        if(self.m_patate >= usine.cout and self.m_sel >= usine.cout):
            self.m_sel -= usine.cout
            self.m_patate -= usine.cout
            self.m_usine.append(usine)

    def newField(self,position):
        champ= Field(position, len(self.m_champs))
        if(self.m_patate >= champ.cout):
            self.m_patate -= champ.cout
            self.m_champs.append(champ)

    def newSalt(self,position):
        salt= Salt(position, len(self.m_salliere))
        if(self.m_sel >= salt.cout):
            self.m_sel -= salt.cout
            self.m_salliere.append(salt)

    def newChips(self,position):
        chips=Chips(position)
        self.m_chips.append(chips)


    def update(self):
        for k in range(len(self.m_champs)):
            self.m_patate+=self.m_champs[k].update()
        for k in range(len(self.m_salliere)):
            self.m_sel+=self.m_salliere[k].update()
        for k in range(len(self.m_usine)):
            chips=self.m_usine[k].update()
            if (chips!= None and len(chips)>0):
                self.m_patate -= self.m_usine[k].conso
                self.m_sel -= self.m_usine[k].conso
                self.m_chips+=chips
        for k in range(len(self.m_chips)):
            self.m_chips[k].update()

    def draw(self,fenetre,position,b):
        for k in range(len(self.m_champs)):
            self.m_champs[k].draw(fenetre,position,b, self)
        for k in range(len(self.m_salliere)):
            self.m_salliere[k].draw(fenetre,position,b, self)
        for k in range(len(self.m_usine)):
            self.m_usine[k].draw(fenetre,position,b)
        for k in range(len(self.m_chips)):
            self.m_chips[k].draw(fenetre,position)
