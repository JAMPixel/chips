import pygame
from pygame.locals import *
import time

#from Chips import *
from Button import *
from Joueur import *
from Ennemy import *

pygame.init()
pygame.font.init()




#Ouverture de la fenetre Pygame (carre : largeur = hauteur)
fenetre = pygame.display.set_mode((1280,720),pygame.FULLSCREEN,32)
menu =1

grass=pygame.image.load("../asset/decor/grass.png")
construc=pygame.image.load("../asset/decor/construction.png")
objectif=pygame.image.load("../asset/Objectif.png")
cam_pos = [0,0]
SPEED_CAM=10
listParticipant=[]
victoire = -1

def collideGeneral(position):
    rect=Rect(position[0]-64,position[1]-64,128,128)
    collide =False
    k=0
    while(not(collide) and k<len(listParticipant)):
        p= listParticipant[k]
        for i in range(len(p.m_champs)):
            collide=collide or rect.colliderect(p.m_champs[i])
        for i in range(len(p.m_salliere)):
            collide=collide or rect.colliderect(p.m_salliere[i])
        for i in range(len(p.m_usine)):
            collide=collide or rect.colliderect(p.m_usine[i])
        k+=1
    return collide

def collideMouse(position):
    collide =[]
    p= listParticipant[0]
    for i in range(len(p.m_champs)):
        if (p.m_champs[i].rect.collidepoint(position)):
            collide=[0,i]
    for i in range(len(p.m_salliere)):
        if(p.m_salliere[i].rect.collidepoint(position)):
            collide=[1,i]
    for i in range(len(p.m_usine)):
        if(p.m_usine[i].rect.collidepoint(position)):
            collide=[2,i]
    return collide

def game():
    buttonfield= Button((80,80),"../asset/HUD/btn+.png","../asset/HUD/btn+hover.png")
    buttonsalt= Button((80,130),"../asset/HUD/btn+.png","../asset/HUD/btn+hover.png")
    buttonfactory= Button((80,180),"../asset/HUD/btn+.png","../asset/HUD/btn+hover.png")

    buttonattcorbo= Button((80,550),"../asset/HUD/btncorbeau-cage.png","../asset/HUD/btncorbeau-cagehover.png")
    buttonattpluie= Button((80,600),"../asset/HUD/btnnuage-cage.png","../asset/HUD/btnnuage-cagehover.png")

    listParticipant.append(Joueur())
    listParticipant.append(Ennemy(listParticipant))

    construction=0
    type_construc=0
    explication=1
    jeu=1

    pygame.font.init()
    font = pygame.font.Font(pygame.font.get_default_font(), 20)

    patate=pygame.image.load("../asset/decor/potato.png")
    sel=pygame.image.load("../asset/decor/Sel.png")
    chip=pygame.image.load("../asset/chips/chipsFace1.png")


    while explication:
        for event in pygame.event.get():
            if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
                explication = 0
                jeu = 0
                return -1
            if event.type == KEYDOWN and event.key == K_RETURN:
                explication = 0
        fenetre.blit(objectif, (0,0))
        pygame.display.flip()


    if jeu:
        for k in range (3):
            fondX=cam_pos[0]//512 - 1
            fondY=cam_pos[1]//512 - 1
            for i in range(fondX,fondX+5):
                for j in range (fondY,fondY+4):
                    fenetre.blit(grass,(i*512-cam_pos[0],j*512-cam_pos[1]))

            fontdepart = pygame.font.Font(pygame.font.get_default_font(), 120)
            textdep = fontdepart.render(str(3-k), 1, (255,0,0))
            fenetre.blit(textdep, (600, 300))
            pygame.display.flip()
            time.sleep(1)

    while jeu:

        pygame.time.Clock().tick(60)

        #detec touches
        for event in pygame.event.get():
            if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
                jeu = 0
                return -1
            elif event.type == KEYDOWN and event.key == K_SPACE:
                cam_pos[0] = 0
                cam_pos[1] = 0

        mous_g,mous_m,mous_d=pygame.mouse.get_pressed()
        mouse_pos=pygame.mouse.get_pos()

        if (mouse_pos[0]<50):
            cam_pos[0]-=SPEED_CAM
        elif (mouse_pos[0]>1230):
            cam_pos[0]+=SPEED_CAM
        if (mouse_pos[1]<50):
            cam_pos[1]-=SPEED_CAM
        elif (mouse_pos[1]>670):
            cam_pos[1]+=SPEED_CAM

        mouse_cam_pos=[cam_pos[0]+mouse_pos[0],cam_pos[1]+mouse_pos[1]]

        if (mous_g):
            if (buttonfield.on_click(mouse_pos)):
                construction = 1
                type_construc=0
            elif (buttonsalt.on_click(mouse_pos)):
                construction = 1
                type_construc=1
            elif (buttonfactory.on_click(mouse_pos)):
                construction = 1
                type_construc=2
            elif (buttonattcorbo.on_click(mouse_pos)):
                p=listParticipant[0]
                if (p.ravenCage):
                    for k in range (p.m_corbeau):
                        per= random.randint(1,len(listParticipant)-1)
                        i=random.randint(0,len(listParticipant[per].m_champs)-1)
                        listParticipant[per].m_champs[i].addCorbeau()
                    p.m_corbeau=0
            elif (buttonattpluie.on_click(mouse_pos)):
                p=listParticipant[0]
                if (p.cloudCage):
                    for k in range (p.m_nuage):
                        per= random.randint(1,len(listParticipant)-1)
                        i=random.randint(0,len(listParticipant[per].m_salliere)-1)
                        listParticipant[per].m_salliere[i].addRain()
                    p.m_nuage=0
            elif(construction==1):
                if (not(collideGeneral(mouse_cam_pos))):
                    if(type_construc==0):
                        listParticipant[0].newField(mouse_cam_pos)
                    elif(type_construc==1):
                        listParticipant[0].newSalt(mouse_cam_pos)
                    elif(type_construc==2):
                        listParticipant[0].newFactory(mouse_cam_pos)
                    construction=0
            else:
                col=collideMouse(mouse_cam_pos)
                if (col!=[]):
                    if col[0] == 0:
                        listParticipant[0].m_champs[col[1]].onClick(fenetre, font, listParticipant[0])
                    elif col[0] == 1:
                        listParticipant[0].m_salliere[col[1]].onClick(fenetre, font, listParticipant[0])

        listParticipant[0].update()
        for k in range(1,len(listParticipant)):
            listParticipant[k].update2(listParticipant)

        if (random.randint(0,500)<=5):
            per= random.randint(0,len(listParticipant)-1)
            if (len(listParticipant[per].m_champs)>2 and not listParticipant[per].scarcrow):
                ch=random.randint(0,len(listParticipant[per].m_champs)-1)
                listParticipant[per].m_champs[ch].addCorbeau()
            elif (listParticipant[per].scarcrow and listParticipant[per].ravenCage):
                listParticipant[per].m_corbeau+=1
        if (random.randint(0,500)<=5):
            per= random.randint(0,len(listParticipant)-1)
            if (len(listParticipant[per].m_salliere)>2 and not listParticipant[per].umbrella):
                ch=random.randint(0,len(listParticipant[per].m_salliere)-1)
                listParticipant[per].m_salliere[ch].addRain()
            elif (listParticipant[per].umbrella and listParticipant[per].cloudCage):
                listParticipant[per].m_nuage+=1


        fondX=cam_pos[0]//512 - 1
        fondY=cam_pos[1]//512 - 1
        for i in range(fondX,fondX+5):
            for j in range (fondY,fondY+4):
                fenetre.blit(grass,(i*512-cam_pos[0],j*512-cam_pos[1]))

        if (construction==1):
            fenetre.blit(construc,(mouse_pos[0]-64,mouse_pos[1]-64))

        for k in range(0,len(listParticipant)):
            listParticipant[k].draw(fenetre,cam_pos,k==0)
            if k>0:
                listParticipant[k].draw2(fenetre, font, chip, k)


        texte_patate = font.render(str(int(listParticipant[0].m_patate)), 1, (255,255,255))
        texte_sel = font.render(str(int(listParticipant[0].m_sel)), 1, (255,255,255))
        texte_chips = font.render(str(len(listParticipant[0].m_chips)), 1, (255,255,255))

        cout_champ = 10
        if(len(listParticipant[0].m_champs)>0):
            cout_champ = listParticipant[0].m_champs[-1].cout+8
        cout_sel = 10
        if(len(listParticipant[0].m_salliere)>0):
            cout_sel = listParticipant[0].m_salliere[-1].cout+8
        cout_usine = 15
        if(len(listParticipant[0].m_usine)>0):
            cout_usine = listParticipant[0].m_usine[-1].cout+8

        texte_cout_champ = font.render(": - " + str(cout_champ), 1, (255,0,0))
        texte_cout_sel = font.render(": - " + str(cout_sel), 1, (255,0,0))
        texte_cout_usine = font.render(": - " + str(cout_usine), 1, (255,0,0))

        fenetre.blit(patate, (5, 65))
        fenetre.blit(sel, (5, 115))
        fenetre.blit(chip, (5, 165))
        fenetre.blit(texte_patate, (30, 65))
        fenetre.blit(texte_sel, (30, 115))
        fenetre.blit(texte_chips, (30, 165))

        fenetre.blit(patate, (100, 65))
        fenetre.blit(sel, (100, 115))
        fenetre.blit(patate, (100, 165))
        fenetre.blit(sel, (100, 195))
        fenetre.blit(texte_cout_champ, (130, 75))
        fenetre.blit(texte_cout_sel, (130, 125))
        fenetre.blit(texte_cout_usine, (130, 175))
        fenetre.blit(texte_cout_usine, (130, 205))

        buttonfield.draw(fenetre)
        buttonsalt.draw(fenetre)
        buttonfactory.draw(fenetre)
        if listParticipant[0].ravenCage:
            buttonattcorbo.draw(fenetre)
            txt_nb_corbeaux = font.render(" : " + str(listParticipant[0].m_corbeau), 1, (255,255,255))
            fenetre.blit(txt_nb_corbeaux, (buttonattcorbo.x+40, buttonattcorbo.y))

        if listParticipant[0].cloudCage:
            buttonattpluie.draw(fenetre)
            txt_nb_nuages = font.render(" : " + str(listParticipant[0].m_nuage), 1, (255,255,255))
            fenetre.blit(txt_nb_nuages, (buttonattpluie.x+40, buttonattpluie.y))

        pygame.display.flip()

        for i in range(len(listParticipant)):
            if(len(listParticipant[i].m_chips)>=500):
                jeu = 0
                if (i == 0):
                    return 1
                else:
                    return 0


victoire=game()
print(victoire)
font_vict = pygame.font.Font(pygame.font.get_default_font(), 120)
if victoire!=-1:
    if victoire == 1:
        text_vict = font_vict.render("Victoire !", 1, (255,0,0))
        fenetre.blit(text_vict, (400, 300))
    elif victoire == 0:
        text_vict = font_vict.render("Defaite !", 1, (255,0,0))
        fenetre.blit(text_vict, (400, 300))

    for k in range(10):
        pygame.display.flip()

        time.sleep(1)


pygame.quit()
