import pygame


class Button(object):

    def __init__(self, position, file, filehover):
        self.button=pygame.image.load(file).convert_alpha()
        self.buttonHover=pygame.image.load(filehover).convert_alpha()
        self.image=pygame.Surface([32,32])
        self.rect=self.image.get_rect(center=(position[0],position[1]))
        self.x=self.rect.x
        self.y=self.rect.y

    def on_click(self,mouse_pos):
        if self.rect.collidepoint(mouse_pos):
            return 1
        else :
            return 0

    def draw(self, fenetre):
        mouse_pos=pygame.mouse.get_pos()
        if self.rect.collidepoint(mouse_pos):
            fenetre.blit(self.buttonHover,(self.x,self.y))
        else:
            fenetre.blit(self.button,(self.x,self.y))
