from Button import *
from Joueur import *


class ButtonFieldAdd(Button):

    def __init__(self, position):
        super().__init__(position,"../asset/HUD/btn+.png","../asset/HUD/btn+hover.png")

    def on_click(self,mouse_pos):
        if self.rect.collidepoint(mouse_pos):
            return 1
        else :
            return 0
