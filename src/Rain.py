import pygame
import random


class Rain(pygame.sprite.Sprite):
    def __init__(self, position):
        pygame.sprite.Sprite.__init__(self)
        self.image=pygame.Surface([32,32])
        self.img=pygame.image.load("../asset/chips/pluie.png").convert_alpha()
        self.img2=pygame.image.load("../asset/chips/pluie2.png").convert_alpha()

        self.rect=self.image.get_rect(center=(position[0],position[1]))
        self.image=self.img
        self.x=self.rect.x
        self.y=self.rect.y


        self.life_time= random.randint(100,150)


    def update(self):
        self.life_time-=1
        if (self.life_time%6<3):
            self.image=self.img2
        else:
            self.image=self.img

        if self.life_time<=0:
            return 1
        else:
            return 0

    def draw(self, fenetre, position):
        fenetre.blit(self.image,(self.x-position[0],self.y-position[1]))
