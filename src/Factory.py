import pygame
import random
from Chips import *

class Factory(pygame.sprite.Sprite):
    def __init__(self, position, nb_bat):
        pygame.sprite.Sprite.__init__(self)

        self.image=pygame.Surface([128,128])
        img=pygame.image.load("../asset/decor/factory.png").convert_alpha()
        self.imgbleu=pygame.Surface([130,130])
        self.imgbleu=pygame.image.load("../asset/decor/fondbleu.png").convert_alpha()
        self.imgrouge=pygame.Surface([130,130])
        self.imgrouge=pygame.image.load("../asset/decor/fondrouge.png").convert_alpha()

        self.rect=self.image.get_rect(center=(position[0],position[1]))
        self.rectbl=self.imgbleu.get_rect(center=(position[0],position[1]))
        self.rectrg=self.imgrouge.get_rect(center=(position[0],position[1]))
        self.image=img
        self.x=self.rect.x
        self.y=self.rect.y

        self.cout = 15 + 8*nb_bat
        self.conso = 3
        self.value = 0.015
        self.new = 0

    def update(self):
        newchips = []
        self.new += self.value
        if(self.new >= 1):
            self.new = 0

            dx = 0
            dy = 0
            while dx == 0 and dy == 0:
                dx = random.randint(0, 2) - 1
                dy = random.randint(0, 2) - 1

            newx = self.x + random.randint(70, 120) * dx
            newy = self.y + random.randint(70, 120) * dy
            newchips.append(Chips((newx, newy)));
        return(newchips)

    def draw(self, fenetre, position,b):
        if(b):
            fenetre.blit(self.imgbleu,(self.x-position[0]-1,self.y-position[1]-1))
        else:
            fenetre.blit(self.imgrouge,(self.x-position[0]-1,self.y-position[1]-1))
        fenetre.blit(self.image,(self.x-position[0],self.y-position[1]))
