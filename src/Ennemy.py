import pygame
from Participant import *
import math


class Ennemy(Participant):
    def __init__(self,listParticipant):
        Participant.__init__(self)
        dist = random.randint(2000,2500)
        angle= random.randint(0,360)
        self.posX= math.cos(angle)*dist
        self.posY= math.sin(angle)*dist
        self.r = 256
        self.nbessai=5
        
        self.essai=0
        self.frame=0
        self.k=0

        pos=self.generateCoord()
        self.newField(pos)
        while (self.collideGeneral(pos,listParticipant)):
            pos=self.generateCoord()
        self.newSalt(pos)




    def update2(self,listParticipant):
        self.update()
        bat = random.randint(0,2)
        pos=self.generateCoord()

        self.frame+=1

        if (self.frame==30):
            self.frame=0
            if(self.collideGeneral(pos,listParticipant)):
                self.k+=1
                if (self.k==self.nbessai):
                    self.r*=1.5
                    self.nbessai+=5
                    self.k=0
            else:
                if (bat==0):
                    self.newField(pos)
                elif (bat==1):
                    self.newSalt(pos)
                elif (bat==2):
                    self.newFactory(pos)
                self.k=0

    def generateCoord(self):
        dist = random.randint(0,self.r)
        angle =random.randint(0,360)
        return [self.posX+math.cos(angle)*dist,self.posY+math.sin(angle)*dist]

    def collideGeneral(self, position,listParticipant):
        rect=pygame.rect.Rect(position[0]-64,position[1]-64,128,128)
        collide =False
        k=0
        while(not(collide) and k<len(listParticipant)):
            p= listParticipant[k]
            for i in range(len(p.m_champs)):
                collide=collide or rect.colliderect(p.m_champs[i])
            for i in range(len(p.m_salliere)):
                collide=collide or rect.colliderect(p.m_salliere[i])
            for i in range(len(p.m_usine)):
                collide=collide or rect.colliderect(p.m_usine[i])
            k+=1
        return collide

    def draw2(self, fenetre, font, img, k):
        texte_chips = font.render(str(len(self.m_chips)), 1, (255,255,255))
        fenetre.blit(img, (1150, 115+k*50))
        fenetre.blit(texte_chips, (1175, 115+k*50))
