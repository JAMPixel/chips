import pygame
import random

proba_change = 0.01

class Chips(pygame.sprite.Sprite):
    def __init__(self, position):
        pygame.sprite.Sprite.__init__(self)

        self.image=pygame.Surface([32,32])
        self.frame_face=pygame.image.load("../asset/chips/chipsFace1.png").convert_alpha()
        self.frame_3_4=pygame.image.load("../asset/chips/chipsTroisQuart1.png").convert_alpha()
        self.frame_profil=pygame.image.load("../asset/chips/chipsCote1.png").convert_alpha()

        self.rect=self.image.get_rect(center=(position[0],position[1]))
        self.image=self.frame_face
        self.dx=0
        self.dy=0
        self.x=self.rect.x
        self.y=self.rect.y
        self.t_mouv = 0


    def update(self):
        r = 1
        if self.dx==0 and self.dy==0:
            r = random.random()

        if(r<proba_change):
            mouv = random.randint(0, 3)
            if(mouv == 0):
                self.dy = 1
                self.image = self.frame_face
            elif(mouv == 1):
                self.dx = -1
                self.image = self.frame_profil
            elif(mouv == 2):
                self.dy = -1
                self.image = self.frame_face
            elif(mouv == 3):
                self.dx = 1
                self.image = self.frame_profil
        else:
            self.x += self.dx
            self.y += self.dy
            self.t_mouv += 1
            if(self.t_mouv >=100):
                self.t_mouv = 0
                self.dx = 0
                self.dy = 0




    def draw(self, fenetre, position):
        fenetre.blit(self.image,(self.x-position[0],self.y-position[1]))
