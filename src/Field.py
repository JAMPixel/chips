import pygame
from pygame.locals import *
from Corbeau import *
from Button import *

class Field(pygame.sprite.Sprite):
    def __init__(self, position, nb_bat):
        pygame.sprite.Sprite.__init__(self)

        self.image=pygame.Surface([128,128])
        img=pygame.image.load("../asset/decor/field.png").convert_alpha()
        self.imgbleu=pygame.Surface([130,130])
        self.imgbleu=pygame.image.load("../asset/decor/fondbleu.png").convert_alpha()
        self.imgrouge=pygame.Surface([130,130])
        self.imgrouge=pygame.image.load("../asset/decor/fondrouge.png").convert_alpha()

        self.rect=self.image.get_rect(center=(position[0],position[1]))
        self.rectbl=self.imgbleu.get_rect(center=(position[0],position[1]))
        self.rectrg=self.imgrouge.get_rect(center=(position[0],position[1]))
        self.image=img
        self.x=self.rect.x
        self.y=self.rect.y

        self.cout = 10 +8*nb_bat
        self.corbeau=None

        self.value = 0.075

    def update(self):
        if (self.corbeau==None):
            return(self.value)
        else:
            if (self.corbeau.update()):
                self.corbeau=None
            return 0


    def draw(self, fenetre, position,b,joueur):
        if(b):
            fenetre.blit(self.imgbleu,(self.x-position[0]-1,self.y-position[1]-1))
        else:
            fenetre.blit(self.imgrouge,(self.x-position[0]-1,self.y-position[1]-1))

        fenetre.blit(self.image,(self.x-position[0],self.y-position[1]))
        if(joueur.scarcrow):
            scarcrow = pygame.image.load("../asset/chips/epouvantail.png").convert_alpha()
            fenetre.blit(scarcrow,(self.x-position[0]+30,self.y-position[1]+30))
        elif (self.corbeau!=None):
            self.corbeau.draw(fenetre,position)

    def addCorbeau(self):
        if (self.corbeau==None):
            self.corbeau=Corbeau([self.x+64,self.y+64])

    def onClick(self, fenetre, font, joueur):
        pause=True
        cout_scarcrow = 20
        cout_cage = 50
        tempo = 0
        max_tempo=15

        if not joueur.scarcrow or not joueur.ravenCage:

            while pause:
                pygame.time.Clock().tick(60)
                tempo+=1
                if tempo>max_tempo:
                    tempo=max_tempo
                for event in pygame.event.get():
                    if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
                        pause = False

                txt_cout = ""
                txt_explication = ""
                txt_explication2 = ""
                txt_explication3 = ""
                txt_explication4 = ""
                panneau = pygame.image.load("../asset/HUD/panneau.png").convert_alpha()
                buttonScarcrow= Button((230,260),"../asset/HUD/btnepouvantail.png","../asset/HUD/btnepouvantailhover.png")
                buttonCage= Button((230,350),"../asset/HUD/btncorbeau-cage.png","../asset/HUD/btncorbeau-cagehover.png")
                imgChips = pygame.image.load("../asset/chips/chipsFace1.png").convert_alpha()

                mous_g,mous_m,mous_d=pygame.mouse.get_pressed()
                mouse_pos=pygame.mouse.get_pos()

                if (mous_g):
                    if joueur.scarcrow == False and buttonScarcrow.on_click(mouse_pos) and len(joueur.m_chips) >= cout_scarcrow:
                        pause = False
                        joueur.scarcrow = True
                        for i in range(cout_scarcrow):
                            joueur.m_chips.pop()
                    elif joueur.ravenCage == False and joueur.scarcrow == True and buttonCage.on_click(mouse_pos) and len(joueur.m_chips) >= cout_cage:
                        pause = False
                        joueur.ravenCage = True
                        for i in range(cout_cage):
                            joueur.m_chips.pop()
                    elif not(panneau.get_rect().collidepoint(mouse_pos)) and tempo==max_tempo:
                        pause = False


                fenetre.blit(panneau, (200, 200))

                if joueur.scarcrow == False:
                    txt_cout = ": -" + str(cout_scarcrow)
                    txt_explication = "L'epouvantail fait fuir"
                    txt_explication2 = "les corbeaux"
                    buttonScarcrow.draw(fenetre)

                    render_cout = font.render(txt_cout, 1, (255,0,0))
                    render_explication = font.render(txt_explication, 1, (255,255,255))
                    render_explication2 = font.render(txt_explication2, 1, (255,255,255))

                    fenetre.blit(imgChips, (260, 240))
                    fenetre.blit(render_cout, (300, 250))
                    fenetre.blit(render_explication, (260, 275))
                    fenetre.blit(render_explication2, (260, 300))

                if joueur.ravenCage == False:
                    txt_cout = ": -" + str(cout_cage)
                    txt_explication = "La cage capture les "
                    txt_explication2 = "corbeaux et permet de"
                    txt_explication3 = "les utiliser contre"
                    txt_explication4 = "l'adversaire"

                    buttonCage.draw(fenetre)

                    render_cout = font.render(txt_cout, 1, (255,0,0))
                    render_explication = font.render(txt_explication, 1, (255,255,255))
                    render_explication2 = font.render(txt_explication2, 1, (255,255,255))
                    render_explication3 = font.render(txt_explication3, 1, (255,255,255))
                    render_explication4 = font.render(txt_explication4, 1, (255,255,255))
                    fenetre.blit(imgChips, (260, 330))
                    fenetre.blit(render_cout, (300, 340))
                    fenetre.blit(render_explication, (260, 365))
                    fenetre.blit(render_explication2, (260, 390))
                    fenetre.blit(render_explication3, (260, 415))
                    fenetre.blit(render_explication4, (260, 440))
                else:
                    txt_cout = ""
                    txt_explication = ""
                    txt_explication2 = ""



                pygame.display.flip()
